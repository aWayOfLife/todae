import { v4 as uuidv4 } from 'uuid';

export class BaseEventDto {
  eventId: string;
  constructor() {
    this.eventId = `event-${uuidv4()}`;
  }
}
