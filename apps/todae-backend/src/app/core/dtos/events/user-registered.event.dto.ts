import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { BaseEventDto } from './base.event.dto';
export class UserRegisteredEventDto implements BaseEventDto {
  eventId: string;
  payload: UserRegisteredEventPayloadDto;
  constructor(payload: UserRegisteredEventPayloadDto) {
    this.payload = payload;
  }
}

export class UserRegisteredEventPayloadDto {
  @IsNotEmpty()
  userId: string;
  @IsNotEmpty()
  @IsEmail()
  userEmail: string;
  @IsString()
  userName: string;
}
