export interface CurrentUser {
  userId: string;
  userEmail: string;
  userName?: string;
}
