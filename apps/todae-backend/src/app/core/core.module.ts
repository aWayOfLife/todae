import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { ThrottlerModule } from '@nestjs/throttler';
import { TypeOrmModule } from '@nestjs/typeorm';
import serverConfiguration from './config/server.configuration';
import { Todo } from './entities/todo.entity';
import { User } from './entities/user.entity';
import { ServerConfig } from './enums';
import { FirebaseAdminHelperService } from './services/firebase-admin-helper.service';
import { SendgridHelperService } from './services/sendgrid-helper.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env.stage.dev'],
      load: [serverConfiguration],
    }),
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'todae.db',
      entities: [User, Todo],
      synchronize: true,
    }),
    ThrottlerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        ttl: config.get(ServerConfig.Throttle).throttleTTL,
        limit: config.get(ServerConfig.Throttle).throttleLimit,
      }),
    }),
    EventEmitterModule.forRoot(),
  ],
  providers: [FirebaseAdminHelperService, SendgridHelperService],
  exports: [FirebaseAdminHelperService, SendgridHelperService],
})
export class CoreModule {}
