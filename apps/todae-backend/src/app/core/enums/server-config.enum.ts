export enum ServerConfig {
  ServiceAccount = 'serviceAccount',
  Sendgrid = 'sendgrid',
  Throttle = 'throttle',
}
