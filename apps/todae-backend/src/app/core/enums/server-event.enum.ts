export enum ServerEvent {
  UserRegistered,
}

export enum ServerEventString {
  UserRegistered = 'user.registered',
}
