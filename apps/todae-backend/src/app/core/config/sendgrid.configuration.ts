import { env } from 'process';
export interface SendGridConfig {
  sendgridApiKey: string;
  sendgridVerifiedFromEmail: string;
  sendgridEnabled: string;
}

export const sendgrid = (): SendGridConfig => {
  return {
    sendgridApiKey: env.SENDGRID_API_KEY,
    sendgridVerifiedFromEmail: env.SENDGRID_VERIFIED_FROM_EMAIL,
    sendgridEnabled: env.SENDGRID_ENABLED,
  } as SendGridConfig;
};
