import { env } from 'process';

export interface ServiceAccountConfig {
  type: string;
  projectId: string;
  privateKeyId: string;
  privateKey: string;
  clientEmail: string;
  clientId: string;
  authUri: string;
  tokenUri: string;
  authProviderX509CertUrl: string;
  clientX509CertUrl: string;
  databaseUrl: string;
  firebaseAppUrl: string;
}

export const serviceAccount = () => {
  return {
    type: env.FIREBASE_TYPE,
    projectId: env.FIREBASE_PROJECT_ID,
    privateKeyId: env.FIREBASE_PRIVATE_KEY_ID.replace(/\\n/gm, '\n'),
    privateKey: env.FIREBASE_PRIVATE_KEY.replace(/\\n/gm, '\n'),
    clientEmail: env.FIREBASE_CLIENT_EMAIL,
    clientId: env.FIREBASE_CLIENT_ID,
    authUri: env.FIREBASE_AUTH_URI,
    tokenUri: env.FIREBASE_TOKEN_URI,
    authProviderX509CertUrl: env.FIREBASE_AUTH_PROVIDER_X509_CERT_URL,
    clientX509CertUrl: env.FIREBASE_CLIENT_X509_CERT_URL,
    databaseUrl: env.FIREBASE_DATABASE_URL,
    firebaseAppUrl: env.FIREBASE_APP_URL,
  } as ServiceAccountConfig;
};
