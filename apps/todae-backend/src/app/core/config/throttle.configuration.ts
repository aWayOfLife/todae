import { env } from 'process';
export interface ThrottleConfig {
  throttleTTL: string;
  throttleLimit: string;
}
export const throttle = () => {
  return {
      throttleTTL: env.THROTTLE_TTL,
      throttleLimit: env.THROTTLE_LIMIT,
    } as ThrottleConfig;
}
