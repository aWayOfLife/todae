import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as SendGrid from '@sendgrid/mail';
import { ServerConfig } from '../../core/enums';

@Injectable()
export class SendgridHelperService {
  constructor(private readonly configService: ConfigService) {
    const sendgridConfiguration = this.configService.get(ServerConfig.Sendgrid);
    SendGrid.setApiKey(sendgridConfiguration.sendgridApiKey);
  }

  async sendEmail(mail: SendGrid.MailDataRequired) {
    const transport = await SendGrid.send(mail);
    return transport;
  }
}
