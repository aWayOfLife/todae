import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';
import { Todo } from './todo.entity';

@Entity()
export class User {
  @PrimaryColumn({
    name: 'user_id',
    type: 'nvarchar',
  })
  userId: string;

  @Column({
    name: 'user_email',
    nullable: false,
    type: 'nvarchar',
  })
  userEmail: string;

  @Column({
    name: 'user_name',
    nullable: false,
    type: 'nvarchar',
  })
  userName: string;

  @Column({
    name: 'user_created_at',
    nullable: false,
    type: 'datetime',
    default: new Date().toISOString(),
  })
  userCreatedAt: string;

  @Column({
    name: 'user_updated_at',
    nullable: false,
    type: 'datetime',
    default: new Date().toISOString(),
  })
  userUpdatedAt: string = new Date().toISOString();

  @OneToMany(() => Todo, (todo) => todo.user)
  todos: Todo[];
}
