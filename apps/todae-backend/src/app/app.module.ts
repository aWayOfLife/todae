import { Module } from '@nestjs/common';

import { AuthModule } from './auth/auth.module';
import { MailingModule } from './mailing/mailing.module';
import { TodoModule } from './todo/todo.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    AuthModule,
    MailingModule,
    TodoModule,
    UserModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
