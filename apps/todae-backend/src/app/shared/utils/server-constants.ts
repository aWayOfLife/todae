export const VERIFICATION_EMAIL_SUBJECT = 'Verify your Todae account';
export const VERIFICATION_EMAIL_TEXT =
  'Please click on the link to verify your account';

export const SWAGGER_TITLE = 'Todae';
export const SWAGGER_DESCRIPTION = 'REST API for Todae Application';
export const SWAGGER_VERSION = '1.0';
