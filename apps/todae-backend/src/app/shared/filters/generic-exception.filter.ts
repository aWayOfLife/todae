import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';

@Catch()
export class GenericExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(GenericExceptionFilter.name);

  catch(exception: Error, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const request = context.getRequest();
    const response = context.getResponse();

    const userAgent = request.get('user-agent') ?? '';
    const { ip, method, url } = request;
    const statusCode =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    this.logger.error(
      `ERROR ${method} ${url} ${userAgent} ${ip}: ${statusCode} ${exception.message} `
    );

    response.status(statusCode).json({
      metadata: {
        timeStamp: new Date().toISOString(),
        path: request.path,
        method: request.method,
      },
      error: {
        status: statusCode,
        message: exception.message,
      },
    });
  }
}
