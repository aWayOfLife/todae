import { Test, TestingModule } from '@nestjs/testing';
import { SendgridHelperService } from './sendgrid-helper.service';

describe('SendgridHelperService', () => {
  let service: SendgridHelperService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SendgridHelperService],
    }).compile();

    service = module.get<SendgridHelperService>(SendgridHelperService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
