import { Injectable, Scope } from '@nestjs/common';
import { CurrentUser } from '../../core/interfaces';

@Injectable({ scope: Scope.REQUEST })
export class AuthSessionService {
  private currentUser: CurrentUser;

  getCurrentUser() {
    return this.currentUser;
  }

  setCurrentUser(currentUser: CurrentUser) {
    this.currentUser = currentUser;
  }
}
