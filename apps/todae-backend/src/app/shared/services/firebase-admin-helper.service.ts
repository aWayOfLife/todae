import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { APP_NAME } from '@todae/todae-core';
import * as admin from 'firebase-admin';
import { ServerConfig } from '../enums';

@Injectable()
export class FirebaseAdminHelperService {
  private app: admin.app.App;
  constructor(private readonly configService: ConfigService) {
    const serviceAccountConfiguration = this.configService.get(
      ServerConfig.ServiceAccount
    );
    this.app = admin.initializeApp(
      {
        credential: admin.credential.cert(serviceAccountConfiguration),
      },
      APP_NAME
    );
  }

  getApp() {
    return this.app;
  }
}
