import { Test, TestingModule } from '@nestjs/testing';
import { FirebaseAdminHelperService } from './firebase-admin-helper.service';

describe('FirebaseAdminHelperService', () => {
  let service: FirebaseAdminHelperService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FirebaseAdminHelperService],
    }).compile();

    service = module.get<FirebaseAdminHelperService>(FirebaseAdminHelperService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
