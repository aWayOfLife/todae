import { AuthSessionService } from './services/auth-session.service';
import { CoreModule } from './../core/core.module';
import { GenericExceptionFilter } from './filters/generic-exception.filter';
import { Module } from '@nestjs/common';
import { ThrottlerGuard } from '@nestjs/throttler';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { LoggingInterceptor } from './interceptors/logging.interceptor';

@Module({
  imports: [CoreModule],
  providers: [
    AuthSessionService,
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: GenericExceptionFilter,
    },
  ],
  exports: [AuthSessionService],
})
export class SharedModule {}
