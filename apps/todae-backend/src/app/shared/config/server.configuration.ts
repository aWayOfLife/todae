import { throttle } from './throttle.configuration';
import { sendgrid } from './sendgrid.configuration';
import { serviceAccount } from './service-account.configuration';
export default () => {
  return {
    serviceAccount: serviceAccount(),
    sendgrid: sendgrid(),
    throttle: throttle(),
  };
};
