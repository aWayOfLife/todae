import { Color, Status } from '@todae/todae-core';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Todo {
  @PrimaryColumn({
    name: 'todo_id',
    type: 'nvarchar',
  })
  todoId: string;

  @Column({
    name: 'todo_title',
    nullable: false,
    type: 'nvarchar',
  })
  todoTitle: string;

  @Column({
    name: 'todo_description',
    nullable: false,
    type: 'nvarchar',
  })
  todoDescription: string;

  @Column({
    name: 'todo_color',
    nullable: false,
    type: 'simple-enum',
    enum: Color,
    default: Color.Red,
  })
  todoColor: Color;

  @Column({
    name: 'todo_status',
    nullable: false,
    type: 'simple-enum',
    enum: Status,
    default: Status.Pending,
  })
  todoStatus: Status;

  @Column({
    name: 'todo_created_at',
    nullable: false,
    type: 'datetime',
    default: new Date().toISOString(),
  })
  todoCreatedAt: string;

  @Column({
    name: 'todo_updated_at',
    nullable: false,
    type: 'datetime',
    default: new Date().toISOString(),
  })
  todoUpdatedAt: string = new Date().toISOString();

  @ManyToOne(() => User, (user) => user.todos, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id' })
  user: User;

  @Column({
    name: 'user_id',
  })
  userId: string;
}
