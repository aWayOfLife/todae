import {
  CallHandler,
  ExecutionContext,
  Injectable,
  Logger,
  NestInterceptor,
  Scope,
} from '@nestjs/common';
import { Observable, tap } from 'rxjs';
import { Request, Response } from 'express';
import { AuthSessionService } from '../services/auth-session.service';

@Injectable({ scope: Scope.REQUEST })
export class LoggingInterceptor implements NestInterceptor {
  private readonly logger = new Logger(LoggingInterceptor.name);

  constructor(private readonly authSessionService: AuthSessionService) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<unknown> {
    const request = context.switchToHttp().getRequest() as Request;
    const userAgent = request.get('user-agent') ?? '';
    const { ip, method, url } = request;
    this.logger.log(
      `REQUEST ${method} ${url} ${userAgent} ${ip} ${
        this.authSessionService.getCurrentUser()?.userId
      }: ${context.getClass().name} ${context.getHandler().name}`
    );

    const now = Date.now();
    return next.handle().pipe(
      tap((data) => {
        const response = context.switchToHttp().getResponse() as Response;
        const { statusCode } = response;
        this.logger.log(
          `RESPONSE ${method} ${url} ${userAgent} ${ip} ${
            this.authSessionService.getCurrentUser()?.userId
          }: ${statusCode} ${JSON.stringify(data)} - ${Date.now() - now}ms`
        );
      })
    );
  }
}
