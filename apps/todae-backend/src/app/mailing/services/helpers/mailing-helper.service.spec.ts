import { Test, TestingModule } from '@nestjs/testing';
import { MailingHelperService } from './mailing-helper.service';

describe('MailingHelperService', () => {
  let service: MailingHelperService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MailingHelperService],
    }).compile();

    service = module.get<MailingHelperService>(MailingHelperService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
