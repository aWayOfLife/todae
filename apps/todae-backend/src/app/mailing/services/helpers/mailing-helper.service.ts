import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import { MailDataRequired } from '@sendgrid/mail';
import { VERIFICATION_EMAIL_SUBJECT, VERIFICATION_EMAIL_TEXT } from '../../../core/utils';
import { ServerConfig } from '../../../core/enums';

@Injectable()
export class MailingHelperService {
  constructor(private readonly configService: ConfigService) {}

  generateVerificationEmail(userEmail: string, verificationLink: string) {
    const verificationMail: MailDataRequired = {
      to: userEmail,
      subject: VERIFICATION_EMAIL_SUBJECT,
      from: this.configService.get(ServerConfig.Sendgrid)
        .sendgridVerifiedFromEmail,
      text: VERIFICATION_EMAIL_TEXT,
      html: `<a>${verificationLink}</a>`,
    };

    return verificationMail;
  }
}
