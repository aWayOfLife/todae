import { ConfigService } from '@nestjs/config';
import { MailingHelperService } from './helpers/mailing-helper.service';
import { AuthService } from './../../auth/services/auth.service';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { ServerConfig, ServerEvent, ServerEventString } from '../../core/enums';
import { UserRegisteredEventDto } from '../../core/dtos';
import { SendgridHelperService } from '../../core/services/sendgrid-helper.service';

@Injectable()
export class MailingService {
  constructor(
    private readonly sendgridHelperService: SendgridHelperService,
    private readonly authService: AuthService,
    private readonly mailingHelperService: MailingHelperService,
    private readonly configService: ConfigService
  ) {}

  @OnEvent(ServerEventString[ServerEvent.UserRegistered])
  async sendVerificationEmail(userRegisteredEventDto: UserRegisteredEventDto) {
    if (
      this.configService.get(ServerConfig.Sendgrid).sendgridEnabled === 'FALSE'
    )
      return;
    const verificationLink =
      await this.authService.generateEmailVerificationLink(
        userRegisteredEventDto.payload.userEmail
      );

    const verificationEmail =
      this.mailingHelperService.generateVerificationEmail(
        userRegisteredEventDto.payload.userEmail,
        verificationLink
      );
    return await this.sendgridHelperService.sendEmail(verificationEmail);
  }
}
