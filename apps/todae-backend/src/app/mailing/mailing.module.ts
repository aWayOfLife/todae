import { SharedModule } from './../shared/shared.module';
import { AuthModule } from './../auth/auth.module';
import { CoreModule } from './../core/core.module';
import { Module } from '@nestjs/common';
import { MailingService } from './services/mailing.service';
import { MailingHelperService } from './services/helpers/mailing-helper.service';
import { MailingController } from './controllers/mailing.controller';

@Module({
  imports: [CoreModule, SharedModule, AuthModule],
  controllers: [MailingController],
  providers: [MailingService, MailingHelperService],
})
export class MailingModule {}
