import { MailingService } from './../services/mailing.service';
import { Controller, Post } from '@nestjs/common';
import { Endpoint } from '@todae/todae-core';
import { Throttle } from '@nestjs/throttler';
import {
  UserRegisteredEventDto,
  UserRegisteredEventPayloadDto,
} from '../../core/dtos';
import { ApiTags } from '@nestjs/swagger';
import { AuthSessionService } from '../../shared/services/auth-session.service';

@ApiTags(Endpoint.Mailing)
@Controller(Endpoint.Mailing)
export class MailingController {
  constructor(
    private readonly mailingService: MailingService,
    private readonly authSessionService: AuthSessionService
  ) {}

  @Throttle(3, 3600)
  @Post('verification')
  sendVerificationEmail() {
    this.mailingService.sendVerificationEmail(
      new UserRegisteredEventDto(
        this.authSessionService.getCurrentUser() as UserRegisteredEventPayloadDto
      )
    );
  }
}
