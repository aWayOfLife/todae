import { SharedModule } from './../shared/shared.module';
import { Todo } from '../core/entities/todo.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TodoController } from './controllers/todo.controller';
import { TodoService } from './services/todo.service';

@Module({
  imports: [TypeOrmModule.forFeature([Todo]), SharedModule],
  controllers: [TodoController],
  providers: [TodoService],
})
export class TodoModule {}
