import { Injectable, Scope } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Status, TodoDto } from '@todae/todae-core';
import { Repository } from 'typeorm';
import { Todo } from '../../core/entities/todo.entity';
import { v4 as uuid4 } from 'uuid';
import { AuthSessionService } from '../../shared/services/auth-session.service';

@Injectable({ scope: Scope.REQUEST })
export class TodoService {
  constructor(
    @InjectRepository(Todo) private readonly todoRepository: Repository<Todo>,
    private readonly authSessionService: AuthSessionService
  ) {}

  async getAllTodos() {
    return await this.todoRepository.find({
      where: {
        user: {
          userId: this.authSessionService.getCurrentUser().userId,
        },
      },
    });
  }

  async getTodoById(id: string) {
    return await this.todoRepository.findOneOrFail({
      where: {
        todoId: id,
        user: {
          userId: this.authSessionService.getCurrentUser().userId,
        },
      },
    });
  }

  async createOrUpdateTodo(todoDto: TodoDto) {
    todoDto.todoId = todoDto.todoId ? todoDto.todoId : uuid4();
    todoDto.userId = todoDto.userId
      ? todoDto.userId
      : this.authSessionService.getCurrentUser().userId;
    return await this.todoRepository.upsert(
      {
        ...todoDto,
        todoId: todoDto.todoId ? todoDto.todoId : uuid4(),
        userId: todoDto.userId
          ? todoDto.userId
          : this.authSessionService.getCurrentUser().userId,
        todoUpdatedAt: new Date().toISOString(),
      },
      {
        conflictPaths: ['todoId'],
      }
    );
  }

  async updateTodoStatus(id: string, status: Status) {
    return await this.todoRepository.update(
      {
        todoId: id,
        user: {
          userId: this.authSessionService.getCurrentUser().userId,
        },
      },
      {
        todoStatus: status,
        todoUpdatedAt: new Date().toISOString(),
      }
    );
  }

  async deleteTodo(id: string) {
    return await this.todoRepository.delete({
      todoId: id,
      user: {
        userId: this.authSessionService.getCurrentUser().userId,
      },
    });
  }
}
