import { TodoService } from './../services/todo.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { Endpoint, Status, TodoDto } from '@todae/todae-core';
import { ApiTags } from '@nestjs/swagger';

@ApiTags(Endpoint.Todo)
@Controller(Endpoint.Todo)
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  async getAllTodos() {
    return await this.todoService.getAllTodos();
  }

  @Get(':id')
  async getTodoById(@Param('id') id: string) {
    return await this.todoService.getTodoById(id);
  }

  @Post()
  async createOrUpdateTodo(@Body() todoDto: TodoDto) {
    return await this.todoService.createOrUpdateTodo(todoDto);
  }

  @Put('status')
  async updateTodoStatus(@Query('id') id: string, @Query('status') status: Status) {
    return await this.todoService.updateTodoStatus(id, status);
  }

  @Delete(':id')
  async deleteTodo(@Param('id') id: string) {
    return await this.todoService.deleteTodo(id);
  }
}
