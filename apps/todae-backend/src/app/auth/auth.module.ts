import { SharedModule } from './../shared/shared.module';
import { AuthMiddleware } from '../shared/middlewares/auth.middleware';
import { AuthService } from './services/auth.service';
import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { AuthController } from './controllers/auth.controller';
import { Endpoint } from '@todae/todae-core';
import { CoreModule } from '../core/core.module';

@Module({
  imports: [CoreModule, SharedModule],
  controllers: [AuthController],
  providers: [AuthService],
  exports: [AuthService],
})
export class AuthModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude(
        { path: 'api/auth/register', method: RequestMethod.ALL },
        { path: Endpoint.Swagger, method: RequestMethod.ALL }
      )
      .forRoutes('*');
  }
}
