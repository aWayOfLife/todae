import { AuthService } from './../services/auth.service';
import {
  Body,
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Endpoint, RegisterDto } from '@todae/todae-core';
import { ApiTags } from '@nestjs/swagger';

@ApiTags(Endpoint.Auth)
@Controller(Endpoint.Auth)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  @UsePipes(ValidationPipe)
  userRegister(@Body() registerDto: RegisterDto) {
    return this.authService.userRegister(registerDto);
  }
}
