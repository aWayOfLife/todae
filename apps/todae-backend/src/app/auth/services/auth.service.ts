import { ConfigService } from '@nestjs/config';
import { LoginDto, RegisterDto } from '@todae/todae-core';
import {
  Injectable,
  UnauthorizedException,
  Scope,
  InternalServerErrorException,
} from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { UserRegisteredEventDto } from '../../core/dtos';
import {
  ServerConfig,
  ServerEvent,
  ServerEventString,
} from '../../core/enums';
import { FirebaseAdminHelperService } from '../../core/services/firebase-admin-helper.service';
import { AuthSessionService } from '../../shared/services/auth-session.service';

@Injectable({ scope: Scope.REQUEST })
export class AuthService {
  constructor(
    private readonly firebaseAdminHelperService: FirebaseAdminHelperService,
    private readonly authSessionService: AuthSessionService,
    private readonly eventEmitter: EventEmitter2,
    private readonly configService: ConfigService
  ) {}

  userLogin(loginDto: LoginDto) {
    return loginDto;
  }

  async userRegister(registerDto: RegisterDto) {
    try {
      const userRecord = await this.firebaseAdminHelperService
        .getApp()
        .auth()
        .createUser({
          email: registerDto.userEmail,
          password: registerDto.userPassword,
          displayName: registerDto.userName,
        });
      this.eventEmitter.emit(
        ServerEventString[ServerEvent.UserRegistered],
        new UserRegisteredEventDto({
          userId: userRecord.uid,
          userEmail: userRecord.email,
          userName: userRecord.displayName,
        })
      );
      return userRecord.uid;
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }
  }

  async generateEmailVerificationLink(userEmail: string) {
    return await this.firebaseAdminHelperService
      .getApp()
      .auth()
      .generateEmailVerificationLink(userEmail, {
        url: this.configService.get(ServerConfig.ServiceAccount).firebaseAppUrl,
      });
  }

  async authenticateUser(token: string) {
    try {
      const decodedToken = await this.firebaseAdminHelperService
        .getApp()
        .auth()
        .verifyIdToken(token);
      this.authSessionService.setCurrentUser({
        userEmail: decodedToken.email,
        userId: decodedToken.uid,
      });
    } catch (exception) {
      throw new UnauthorizedException();
    }
  }
}
