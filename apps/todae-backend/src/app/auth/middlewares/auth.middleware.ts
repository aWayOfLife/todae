import { AuthService } from './../services/auth.service';
import {
  Injectable,
  NestMiddleware,
  UnauthorizedException,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(
    private readonly authService: AuthService
  ) {}
  async use(req: Request, res: Response, next: () => void) {
    const token = req.headers.authorization;
    if (!token) {
      throw new UnauthorizedException();
    }
    await this.authService.authenticateUser(token.replace('Bearer ', ''));
    next();
  }
}
