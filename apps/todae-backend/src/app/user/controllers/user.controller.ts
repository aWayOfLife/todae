import { Body, Controller, Get, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Endpoint, UserDto } from '@todae/todae-core';
import { UserService } from '../services/user.service';

@ApiTags(Endpoint.User)
@Controller(Endpoint.User)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  async getUser() {
    return await this.userService.getUser();
  }

  @Put()
  async updateUser(@Body() userDto: UserDto) {
    return await this.userService.updateUser(userDto);
  }
}
