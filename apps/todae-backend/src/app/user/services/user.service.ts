import { Injectable, Scope } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../core/entities/user.entity';
import { OnEvent } from '@nestjs/event-emitter';
import { ServerEvent, ServerEventString } from '../../core/enums';
import { UserRegisteredEventDto } from '../../core/dtos';
import { UserDto } from '@todae/todae-core';
import { AuthSessionService } from '../../shared/services/auth-session.service';

@Injectable({ scope: Scope.REQUEST })
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly authSessionService: AuthSessionService
  ) {}

  @OnEvent(ServerEventString[ServerEvent.UserRegistered])
  async createUser(userRegisteredEventDto: UserRegisteredEventDto) {
    const user = await this.userRepository.create({
      ...userRegisteredEventDto.payload,
      userUpdatedAt: new Date().toISOString(),
    });
    return await this.userRepository.save(user);
  }

  async getUser() {
    return await this.userRepository.findOneOrFail({
      where: {
        userId: this.authSessionService.getCurrentUser().userId,
      },
    });
  }

  async updateUser(userDto: UserDto) {
    return await this.userRepository.update(
      {
        userId: this.authSessionService.getCurrentUser().userId,
      },
      {
        ...userDto,
        userUpdatedAt: new Date().toISOString(),
      }
    );
  }
}
