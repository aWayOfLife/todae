
/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Endpoint, GLOBAL_PREFIX } from '@todae/todae-core';

import { AppModule } from './app/app.module';
import { SWAGGER_DESCRIPTION, SWAGGER_TITLE, SWAGGER_VERSION } from './app/core/utils';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const globalPrefix = GLOBAL_PREFIX;
  app.setGlobalPrefix(globalPrefix);
  const port = process.env.PORT || 3333;

  const config = new DocumentBuilder()
    .setTitle(SWAGGER_TITLE)
    .setDescription(SWAGGER_DESCRIPTION)
    .setVersion(SWAGGER_VERSION)
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(Endpoint.Swagger, app, document);

  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${globalPrefix}`
  );
}

bootstrap();
