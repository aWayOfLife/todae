export enum Status {
  Pending,
  Done
}

export enum StatusString {
  Pending = 'Pending',
  Done = 'Done'
}
