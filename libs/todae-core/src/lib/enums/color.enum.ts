export enum Color {
  Red,
  Blue,
  Green,
  Violet,
  Orange,
}

export enum ColorString {
  Red = '#FF7070',
  Blue = '#40C4FF',
  Green = '#1DE9B6',
  Violet = '#7C4DFF',
  Orange = '#FFB74D',
}
