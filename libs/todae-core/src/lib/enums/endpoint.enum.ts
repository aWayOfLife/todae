export enum Endpoint {
  Auth = 'auth',
  User = 'user',
  Todo = 'todo',
  Mailing = 'mailing',
  Swagger = 'document',
}