import { IsEnum, IsString } from 'class-validator';
import { Color, Status } from '../enums';

export class TodoDto {
  @IsString()
  todoId: string;
  @IsString()
  userId: string;
  @IsString()
  todoTitle: string;
  @IsString()
  todoDescription: string;
  @IsEnum(Color)
  todoColor: Color;
  @IsEnum(Status)
  todoStatus: Status;
}
