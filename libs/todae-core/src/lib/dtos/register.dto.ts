import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MinLength,
  ValidateIf,
  Matches,
} from 'class-validator';
import { IsEqualTo } from '../decorators';
import { PASSWORD_REGEX } from '../utils';

export class RegisterDto {
  @IsNotEmpty()
  @IsEmail()
  userEmail: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @Matches(PASSWORD_REGEX, {
    message:
      'Password must have at least 1 special character and 1 uppercase character',
  })
  userPassword: string;

  @ValidateIf((registerDto) => registerDto.userPassword)
  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @IsEqualTo(RegisterDto, (registerDto) => registerDto.userPassword, {
    message: 'UserPassword and UserConfirmPassword do not match',
  })
  userConfirmPassword: string;

  @IsNotEmpty()
  @IsString()
  userName: string;
}
