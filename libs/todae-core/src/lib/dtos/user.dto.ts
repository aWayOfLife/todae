import { IsNotEmpty, IsString } from 'class-validator';

export class UserDto {
  @IsNotEmpty()
  @IsString()
  userId: string;
  @IsString()
  userEmail: string;
  @IsString()
  userName: string;
}
