export const PASSWORD_REGEX =
  /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/gm;

export const APP_NAME = 'Todae';

export const GLOBAL_PREFIX = 'api';
